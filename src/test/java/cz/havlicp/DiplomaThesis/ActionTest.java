package cz.havlicp.DiplomaThesis;

import cz.havlicp.DiplomaThesis.controller.ActionsController;
import cz.havlicp.DiplomaThesis.controller.RegionsController;
import cz.havlicp.DiplomaThesis.model.Action;
import cz.havlicp.DiplomaThesis.utils.CreateEntities;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
public class ActionTest {

    @Autowired
    ActionsController actionsController;

    @Autowired
    RegionsController regionsController;

    @Test
    public void testGroup(){
        Action action = CreateEntities.createAction();

        Action savedAction = actionsController.create(action);
        Assert.assertNotNull(savedAction);

        Action newAction = actionsController.findOne(savedAction.getId());
        Assert.assertNotNull(newAction);
        Assert.assertEquals(savedAction.getId(), newAction.getId());

        Action deletedAction = actionsController.delete(newAction.getId());
        Assert.assertNotNull(deletedAction);

        Action checkDelete = actionsController.findOne(deletedAction.getId());
        Assert.assertNull(checkDelete);
    }

    @Test
    public void testFindAll(){
        List<Action> actions = actionsController.findAll();
        assertThat(actions.size(), is(greaterThanOrEqualTo(0)));
    }
}
