package cz.havlicp.DiplomaThesis.utils;

import cz.havlicp.DiplomaThesis.model.*;

public class CreateEntities {

    public static Association createAssociation(){
        Association item = new Association();
        item.setId(1L);
        item.setName("České sdružení");
        return  item;
    }

    public static Region createRegion(){
        Region item = new Region();
        item.setId(1L);
        item.setName("Zlatý střed");
        item.setAssociation(createAssociation());
        return  item;
    }


    public static Group createGroup(){
        Group group = new Group();
        group.setName("name");
        group.setRegion(createRegion());
        return  group;
    }

    public static Action createAction(){
        Action action = new Action();
        action.setName("name");
        action.setRegion(createRegion());
        return  action;
    }

    public static User createUser(){
        User user = new User();
        user.setUsername("name");
        user.setEmail("aaa@aaa.cz");
        user.setPassword("pass123");
        return  user;
    }

}
