package cz.havlicp.DiplomaThesis;

import cz.havlicp.DiplomaThesis.controller.GroupsController;
import cz.havlicp.DiplomaThesis.controller.RegionsController;
import cz.havlicp.DiplomaThesis.model.Group;
import cz.havlicp.DiplomaThesis.utils.CreateEntities;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
public class GroupTest {

    @Autowired
    GroupsController groupsController;

    @Autowired
    RegionsController regionsController;

    @Test
    public void testGroup(){
        Group group = CreateEntities.createGroup();

        Group savedGroup = groupsController.create(group);
        Assert.assertNotNull(savedGroup);

        Group newGroup = groupsController.findOne(savedGroup.getId());
        Assert.assertNotNull(newGroup);
        Assert.assertEquals(savedGroup.getId(), newGroup.getId());

        Group deletedGroup = groupsController.delete(newGroup.getId());
        Assert.assertNotNull(deletedGroup);

        Group checkDelete = groupsController.findOne(deletedGroup.getId());
        Assert.assertNull(checkDelete);
    }

    @Test
    public void testFindAll(){
        List<Group> groups = groupsController.findAll();
        assertThat(groups.size(), is(greaterThanOrEqualTo(0)));
    }
}
