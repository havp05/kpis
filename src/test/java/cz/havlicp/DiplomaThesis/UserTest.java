package cz.havlicp.DiplomaThesis;

import cz.havlicp.DiplomaThesis.controller.GroupsController;
import cz.havlicp.DiplomaThesis.controller.MembersController;
import cz.havlicp.DiplomaThesis.model.Group;
import cz.havlicp.DiplomaThesis.model.User;
import cz.havlicp.DiplomaThesis.utils.CreateEntities;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
public class UserTest {

    @Autowired
    private MembersController membersController;

    @Autowired
    private GroupsController groupsController;
    

    @Test
    public void testGroup(){
        User user = CreateEntities.createUser();
        Group group = groupsController.create(CreateEntities.createGroup());
        user.setGroup(group);
        

        User savedUser = membersController.create(user);
        Assert.assertNotNull(savedUser);

        User newUser = membersController.findOne(savedUser.getId());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(savedUser.getId(), newUser.getId());

        User deletedUser = membersController.delete(newUser.getId());
        Assert.assertNotNull(deletedUser);

        User checkDelete = membersController.findOne(deletedUser.getId());
        Assert.assertNull(checkDelete);

        groupsController.delete(group.getId());
    }


    @Test
    public void testFindAll(){
        List<User> users = membersController.findAll();
        assertThat(users.size(), is(greaterThanOrEqualTo(0)));
    }
}
