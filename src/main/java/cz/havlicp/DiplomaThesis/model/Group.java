package cz.havlicp.DiplomaThesis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "groups")
public class Group {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 50)
    @Column
    private String name;


    @Size(min = 2, max = 50)
    @Column
    private String city;


    @Column(name = "registration_year")
    private String registrationYear;

    @Column
    private String web;


    @Column(name = "note")
    private String description;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
}
