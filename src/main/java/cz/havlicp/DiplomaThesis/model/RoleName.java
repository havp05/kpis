package cz.havlicp.DiplomaThesis.model;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_LEADER,
    ROLE_USER,

}
