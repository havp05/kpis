package cz.havlicp.DiplomaThesis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 50)
    @Column
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    @Column
    private String email;

    @NotBlank
    @Size(min = 6, max = 100)
    private String password;


    @Size(min = 3, max = 50)
    @Column
    private String firstname;


    @Size(min = 3, max = 50)
    @Column
    private String lastname;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_to_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();


    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @Column
    private String address;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column
    private String mobile;

    @Column
    private boolean vegetarian;

    @Column
    private String note;

    @Column(name = "registration_card")
    private boolean registrationCard;

    @Column(name = "photo_agreement")
    private boolean photoAgree;

    @ManyToMany(mappedBy = "members")
    @JsonIgnoreProperties(value = "members")
    private Set<Action> actions;


    public User(String username, String email, String password) {
        //this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

}
