package cz.havlicp.DiplomaThesis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "actions")
public class Action {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 50)
    @Column
    private String name;

    @Column(name = "from_date")
    private LocalDate startDate;

    @Column(name = "to_date")
    private LocalDate endDate;

    @Column(name = "sign_end_date")
    private LocalDate signEndDate;

    @Column(name = "max_people")
    private int maxPeople;

    @Column(name = "member_price")
    private double memberPrice;

    @Column(name = "nonmember_price")
    private double nonMemberPrice;

    @Column
    private String description;

    @Column(name = "contact_person")
    private String contactPerson;

    @ManyToOne
    @JoinColumn(name = "region_organizer_id")
    private Region region;

    @Column
    private String address;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_on_actions",
            joinColumns = @JoinColumn(name = "action_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    @JsonIgnoreProperties(value = {"actions"})
    private Set<User> members = new HashSet<>();

}
