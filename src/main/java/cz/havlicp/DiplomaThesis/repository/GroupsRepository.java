package cz.havlicp.DiplomaThesis.repository;

import cz.havlicp.DiplomaThesis.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupsRepository extends JpaRepository<Group, Long> {
    Group findByName(String name);
}
