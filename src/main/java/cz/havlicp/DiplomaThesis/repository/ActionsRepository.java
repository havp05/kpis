package cz.havlicp.DiplomaThesis.repository;

import cz.havlicp.DiplomaThesis.model.Action;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionsRepository extends JpaRepository<Action, Long> {
    Action findByName(String name);
}
