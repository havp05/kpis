package cz.havlicp.DiplomaThesis.repository;

import cz.havlicp.DiplomaThesis.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionsRepository extends JpaRepository<Region, Long> {

    Region findByName(String name);
}
