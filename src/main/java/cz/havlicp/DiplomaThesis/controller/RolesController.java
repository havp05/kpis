package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.Role;
import cz.havlicp.DiplomaThesis.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping({"/role"})
public class RolesController {

    @Autowired
    RolesService rolesService;

    @GetMapping
    public List<Role> findAll() {
        return rolesService.findAll();
    }
}
