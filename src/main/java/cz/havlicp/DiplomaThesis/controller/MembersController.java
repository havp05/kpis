package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.User;
import cz.havlicp.DiplomaThesis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping({"/users"})
public class MembersController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder encoder;


    @PostMapping
    public User create(@RequestBody User user) {
        if (user.getPassword() != null && !user.getPassword().isEmpty()) {
            user.setPassword(encoder.encode(user.getPassword()));
        }
        return userService.create(user);
    }

    @GetMapping(path = {"/{id}"})
    public User findOne(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @PutMapping(path = {"/{id}"})
    public User update(@PathVariable("id") Long id, @RequestBody User user) {
        user.setId(id);
        return userService.update(user);
    }

    @DeleteMapping(path = {"/{id}"})
    public User delete(@PathVariable("id") Long id) {
        return userService.delete(id);
    }

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping(path = "/exceptsuperadmin")
    public List<User> findAllButSuperadmin() {
        List<User> usersExceptSuperadmin = userService.findAll();
        User superadmin = userService.findById(1L);
        usersExceptSuperadmin.remove(superadmin);
        return usersExceptSuperadmin;
    }
}
