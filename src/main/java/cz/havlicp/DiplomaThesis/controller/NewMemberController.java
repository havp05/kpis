package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.User;
import cz.havlicp.DiplomaThesis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping({"/novyclen"})
public class NewMemberController {

    @Autowired
    UserService userService;

    @PostMapping
    public User create(@RequestBody User user) {
        return userService.create(user);
    }
}
