package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.Group;
import cz.havlicp.DiplomaThesis.service.GroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping({"/oddily"})
public class GroupsController {

    @Autowired
    GroupsService groupsService;

    @GetMapping
    public List<Group> findAll() {
        return groupsService.findAll();
    }


    @PostMapping
    public Group create(@RequestBody Group action) {
        return groupsService.create(action);
    }

    @GetMapping(path = {"/{id}"})
    public Group findOne(@PathVariable("id") Long id) {
        return groupsService.findById(id);
    }

    @PutMapping(path = {"/{id}"})
    public Group update(@PathVariable("id") Long id, @RequestBody Group action) {
        action.setId(id);
        return groupsService.update(action);
    }

    @DeleteMapping(path = {"/{id}"})
    public Group delete(@PathVariable("id") Long id) {
        return groupsService.delete(id);
    }

}
