package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.Region;
import cz.havlicp.DiplomaThesis.service.RegionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping({"/oblasti"})
public class RegionsController {

    @Autowired
    RegionsService regionsService;

    @GetMapping
    public List<Region> getAllRegions() {
        return regionsService.findAll();
    }
}
