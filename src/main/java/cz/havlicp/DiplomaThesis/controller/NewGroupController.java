package cz.havlicp.DiplomaThesis.controller;

import cz.havlicp.DiplomaThesis.model.Group;
import cz.havlicp.DiplomaThesis.model.Region;
import cz.havlicp.DiplomaThesis.service.GroupsService;
import cz.havlicp.DiplomaThesis.service.RegionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping({"/novyoddil"})
public class NewGroupController {

    @Autowired
    GroupsService groupsService;

    @Autowired
    RegionsService regionsService;

    @PostMapping
    public Group create(@RequestBody Group group) {
        return groupsService.create(group);
    }

    @GetMapping
    public List<Region> getAllRegions() {
        return regionsService.findAll();
    }
}
