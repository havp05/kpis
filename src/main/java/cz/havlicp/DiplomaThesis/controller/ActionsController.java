package cz.havlicp.DiplomaThesis.controller;


import cz.havlicp.DiplomaThesis.model.Action;
import cz.havlicp.DiplomaThesis.model.User;
import cz.havlicp.DiplomaThesis.service.ActionsService;
import cz.havlicp.DiplomaThesis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping({"/akce"})
public class ActionsController {

    @Autowired
    ActionsService actionsService;

    @Autowired
    UserService userService;


    @PostMapping
    public Action create(@RequestBody Action action) {
        return actionsService.create(action);
    }

    @GetMapping(path = {"/{id}"})
    public Action findOne(@PathVariable("id") Long id) {
        return actionsService.findById(id);
    }

    @PutMapping(path = {"/{id}"})
    public Action update(@PathVariable("id") Long id, @RequestBody Action action) {
        action.setId(id);
        return actionsService.update(action);
    }

    @DeleteMapping(path = {"/{id}"})
    public Action delete(@PathVariable("id") Long id) {
        return actionsService.delete(id);
    }

    @GetMapping
    public List<Action> findAll() {
        return actionsService.findAll();
    }

    @GetMapping(path = "/userActions/{username}")
    public List<Action> findActionsByUser(@PathVariable("username") String username) {
        List<Action> allActions = actionsService.findAll();
        User user = userService.findByUsername(username);
        return allActions.stream().filter(action -> action.getMembers().contains(user)).collect(Collectors.toList());
    }
}
