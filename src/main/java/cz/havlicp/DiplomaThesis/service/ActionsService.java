package cz.havlicp.DiplomaThesis.service;

import cz.havlicp.DiplomaThesis.model.Action;
import cz.havlicp.DiplomaThesis.repository.ActionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActionsService {

    @Autowired
    private ActionsRepository repository;

    public Action create(Action action) {
        return repository.save(action);
    }

    public Action delete(Long id) {
        Action action = findById(id);
        if (action != null) {
            repository.delete(action);
        }
        return action;
    }


    public Action findById(Long id) {
        Optional<Action> opt = repository.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    public Action update(Action action) {
        return repository.save(action);
    }

    public List<Action> findAll() {
        return repository.findAll();
    }
}
