package cz.havlicp.DiplomaThesis.service;

import cz.havlicp.DiplomaThesis.model.Role;
import cz.havlicp.DiplomaThesis.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolesService {

    @Autowired
    RoleRepository repository;

    public List<Role> findAll() {
        return repository.findAll();
    }
}
