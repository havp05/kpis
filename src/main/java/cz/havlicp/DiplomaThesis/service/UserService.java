package cz.havlicp.DiplomaThesis.service;

import cz.havlicp.DiplomaThesis.model.User;
import cz.havlicp.DiplomaThesis.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User create(User user) {
        return repository.save(user);
    }

    public User delete(Long id) {
        User user = findById(id);
        if (user != null) {
            repository.delete(user);
        }
        return user;
    }


    public User findById(Long id) {
        Optional<User> opt = repository.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    public User update(User user) {
        return repository.save(user);
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    public User findByUsername(String username) {
        return repository.findByUsername(username).get();
    }
}
