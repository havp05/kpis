package cz.havlicp.DiplomaThesis.service;

import cz.havlicp.DiplomaThesis.model.Group;
import cz.havlicp.DiplomaThesis.repository.GroupsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GroupsService {

    @Autowired
    private GroupsRepository repository;

    public Group create(Group group) {
        return repository.save(group);
    }

    public Group delete(Long id) {
        Group group = findById(id);
        if (group != null) {
            repository.delete(group);
        }
        return group;
    }


    public Group findById(Long id) {
        Optional<Group> opt = repository.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    public Group update(Group group) {
        return repository.save(group);
    }

    public List<Group> findAll() {
        return repository.findAll();
    }
}
