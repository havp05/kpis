package cz.havlicp.DiplomaThesis.service;


import java.util.List;

public interface BaseService<T> {

    T create(T item);

    T delete(int id);

    List<T> findAll();

    T findById(int id);

    T update(T item);
}
