import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSnackBarModule
} from '@angular/material';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatMenuModule} from '@angular/material/menu';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {HomeComponent} from './components/home/home.component';


import {AuthGuard} from './auth/auth.guard';
import {GroupsComponent} from './components/groups/groups.component';
import {MembersComponent} from './components/members/members.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {ActionsComponent} from './components/actions/actions.component';
import {NewGroupComponent} from './components/new-group/new-group.component';

import {httpInterceptorProviders} from './auth/auth-interceptor';
import {NewMemberComponent} from './components/new-member/new-member.component';
import {NewActionComponent} from './components/new-action/new-action.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './components/header/header.component';
import {LoginLayoutComponent} from './components/layouts/login-layout/login-layout.component';
import {HomeLayoutComponent} from './components/layouts/home-layout/home-layout.component';
import {SnacbarLoginComponent} from './components/snackbars/snacbar-login/snacbar-login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    GroupsComponent,
    MembersComponent,
    RegistrationComponent,
    ActionsComponent,
    NewGroupComponent,
    NewMemberComponent,
    NewActionComponent,
    HeaderComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    SnacbarLoginComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    [BrowserAnimationsModule],
    [MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatCardModule, MatFormFieldModule,
      MatSelectModule, MatTableModule, MatSortModule, MatPaginatorModule, MatMenuModule],
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatSnackBarModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [AuthGuard, httpInterceptorProviders],
  bootstrap: [AppComponent],
  exports: [SnacbarLoginComponent],
  entryComponents: [SnacbarLoginComponent]
})
export class AppModule {
}
