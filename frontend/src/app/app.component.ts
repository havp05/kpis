import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from './auth/token-storage.service';
import {MembersService} from './services/members.service';
import {Member} from './Model/member';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private roles: string[];
  private authority: string;
  private isLoggedIn: boolean = false;

  members: Member[];
  user: Member;

  constructor(private tokenStorage: TokenStorageService, private memberService: MembersService, private router: Router) {

  }

  ngOnInit() {
    debugger;
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        if (role === 'ROLE_LEADER') {
          this.authority = 'leader';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }


}
