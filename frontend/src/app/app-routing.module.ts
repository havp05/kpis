import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';

import {AuthGuard} from './auth/auth.guard';
import {GroupsComponent} from './components/groups/groups.component';
import {MembersComponent} from './components/members/members.component';
import {ActionsComponent} from './components/actions/actions.component';
import {NewGroupComponent} from './components/new-group/new-group.component';
import {NewMemberComponent} from './components/new-member/new-member.component';
import {NewActionComponent} from './components/new-action/new-action.component';
import {HomeLayoutComponent} from './components/layouts/home-layout/home-layout.component';
import {LoginLayoutComponent} from './components/layouts/login-layout/login-layout.component';

const routes: Routes = [

  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'oddily',
        component: GroupsComponent,
      },
      {
        path: 'clenove',
        component: MembersComponent,
      },
      {
        path: 'akce',
        component: ActionsComponent,
      },
      {
        path: 'novyoddil',
        component: NewGroupComponent,
      },
      {
        path: 'novyoddil/:id',
        component: NewGroupComponent,
      },
      {
        path: 'novyclen',
        component: NewMemberComponent,
      },
      {
        path: 'novyclen/:id',
        component: NewMemberComponent,
      },
      {
        path: 'novaakce',
        component: NewActionComponent,
      },
      {
        path: 'novaakce/:id',
        component: NewActionComponent,
      },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  },


  /*{
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'registrace',
    component: RegistrationComponent,
    canActivate: [AuthGuard]
  },*/



  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
