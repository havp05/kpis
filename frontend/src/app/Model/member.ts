import {Group} from './group';
import {Role} from './role';
import {Action} from './action';

export class Member {
  id?: number;
  username?: string;
  password?: string;
  firstname?: string;
  lastname?: string;
  city?: string;
  address?: string;
  group?: Group;
  birthDate?: Date;
  mobile?: string;
  email?: string;
  note?: string;
  vegetarian?: boolean;
  registrationCard?: boolean;
  photoAgree?: boolean;
  roles?: Role[];
  actions?: Action[];

}
