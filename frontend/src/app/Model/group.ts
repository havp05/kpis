import {Region} from './region';

export class Group {
  id?: number;
  name?: string;
  registrationYear?: string;
  city?: string;
  region?: Region;
  web?: string;
  description?: string;
}
