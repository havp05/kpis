import {Association} from './association';

export class Region {
  id?: number;
  name?: string;
  association?: Association;
}
