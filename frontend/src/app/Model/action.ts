import {Member} from './member';
import {Region} from './region';


export class Action {
  id?: number;
  name?: string;
  startDate?: Date;
  endDate?: Date;
  signEndDate?: Date;
  maxPeople?: number;
  memberPrice?: number;
  nonMemberPrice?: number;
  description?: string;
  contactPerson?: string;
  region?: Region;
  address?: string;
  members?: Member[];
}
