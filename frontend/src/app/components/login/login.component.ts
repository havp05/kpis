import {Component, OnInit} from '@angular/core';

import {AuthService} from '../../auth/auth.service';
import {TokenStorageService} from '../../auth/token-storage.service';
import {AuthLoginInfo} from '../../auth/login-info';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SnacbarLoginComponent} from '../snackbars/snacbar-login/snacbar-login.component';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    username: new FormControl('', {
      validators: Validators.required
    }),
    password: new FormControl('', {
      validators: Validators.required
    }),
  });
  private formSubmitAttempt: boolean;

  isLoggedIn = false;
  isLoginFailed = false;
  roles: string[] = [];
  private loginInfo: AuthLoginInfo;
  returnUrl: string;

  constructor(private snackBar: MatSnackBar, private authService: AuthService, private tokenStorage: TokenStorageService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
    }

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    console.log(this.form);

    if (this.form.valid) {
      this.loginInfo = new AuthLoginInfo(
        this.form.controls.username.value,
        this.form.controls.password.value);

      this.authService.attemptAuth(this.loginInfo).subscribe(
        data => {
          debugger;
          this.tokenStorage.saveToken(data.token);
          this.tokenStorage.saveUsername(data.username);
          this.tokenStorage.saveAuthorities(data.authorities);
          //this.saveUserToStorage();
          debugger;

          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.roles = this.tokenStorage.getAuthorities();
          this.reloadPage();
        },
        error => {
          this.openSnackBar();
          console.log(error);
          this.isLoginFailed = true;
        }
      );
    }
    this.formSubmitAttempt = true;
  }

  reloadPage() {
    this.router.navigate([this.returnUrl]);


  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  openSnackBar() {
    this.snackBar.openFromComponent(SnacbarLoginComponent, {
      duration: 5000,
      verticalPosition: 'top',
    });
  }
}
