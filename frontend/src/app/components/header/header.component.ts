import {Component, OnInit} from '@angular/core';
import {Member} from '../../Model/member';
import {TokenStorageService} from '../../auth/token-storage.service';
import {MembersService} from '../../services/members.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private roles: string[];
  private authority: string;
  private isLoggedIn: boolean = false;

  members: Member[];
  user: Member;

  constructor(private tokenStorage: TokenStorageService, private memberService: MembersService, private router: Router) {

  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        if (role === 'ROLE_LEADER') {
          this.authority = 'leader';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
    this.tokenStorage.setMainRole(this.authority);
  }

  isAdmin(): boolean {
    return this.authority == 'admin';
  }

  openProfile() {

    this.memberService.getAllMembers().subscribe(
      data => {
        debugger;
        this.members = data;
        this.user = this.members.find(m => m.username == this.tokenStorage.getUsername());
        this.router.navigate(['/novyclen', this.user.id]);
      }
    );
  }

  logout() {
    this.tokenStorage.signOut();
    window.location.reload();
  }

}
