import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MembersService} from '../../services/members.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TokenStorageService} from '../../auth/token-storage.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  displayedColumns: string[] = ['fullName', 'group', 'address', 'birthDate', 'mobile', 'email', 'vegetarian', 'note'];
  dataSource: MatTableDataSource<any>;

  isUser: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private membersService: MembersService, private changeDetectorRefs: ChangeDetectorRef,
              private tokenStorage: TokenStorageService) {
    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }
  }

  ngOnInit() {

    this.membersService.getAllMembersExceptSuperadmin().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeDetectorRefs.detectChanges();
        debugger;
      }
    );
    debugger;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
