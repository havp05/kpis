import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ActionsService} from '../../services/actions.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../auth/token-storage.service';


@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'region', 'startDate', 'endDate', 'address', 'signIn', 'maxPeople'];
  dataSource: MatTableDataSource<any>;

  isUser: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private actionsService: ActionsService, private router: Router, private changeDetectorRefs: ChangeDetectorRef,
              private tokenStorage: TokenStorageService) {
    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }
  }

  ngOnInit() {

    this.actionsService.getAllActions().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeDetectorRefs.detectChanges();
        debugger;
      }
    );
    debugger;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
