import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SnacbarLoginComponent} from './snacbar-login.component';

describe('SnacbarLoginComponent', () => {
  let component: SnacbarLoginComponent;
  let fixture: ComponentFixture<SnacbarLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SnacbarLoginComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnacbarLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
