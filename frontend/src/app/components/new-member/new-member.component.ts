import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupsService} from '../../services/groups.service';
import {Group} from '../../Model/group';
import {MembersService} from '../../services/members.service';
import {Role} from '../../Model/role';
import {RolesService} from '../../services/roles.service';
import {Member} from '../../Model/member';
import {TokenStorageService} from '../../auth/token-storage.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-member',
  templateUrl: './new-member.component.html',
  styleUrls: ['./new-member.component.scss']
})
export class NewMemberComponent implements OnInit {

  isUser: boolean = false;

  constructor(private formBuilder: FormBuilder, private tokenStorage: TokenStorageService,
              private groupsService: GroupsService, private memberService: MembersService,
              private rolesService: RolesService, private router: Router,
              private route: ActivatedRoute) {
    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }
  }

  form = new FormGroup({
    id: new FormControl(),
    username: new FormControl('', {
      validators: Validators.required,
    }),
    password: new FormControl('', {
      validators: Validators.required,
    }),
    firstname: new FormControl('', {
      validators: Validators.required
    }),
    lastname: new FormControl('', {
      validators: Validators.required
    }),
    city: new FormControl('', {}),
    address: new FormControl('', {}),
    group: new FormControl('', {
      validators: Validators.required
    }),
    birthDate: new FormControl('', {
      validators: Validators.required
    }),
    mobile: new FormControl('', {}),
    email: new FormControl('', {
      validators: Validators.compose([Validators.required, Validators.email]),
    }),
    note: new FormControl('', {}),
    vegetarian: new FormControl('', {}),
    registrationCard: new FormControl('', {}),
    photoAgree: new FormControl('', {}),
    roles: new FormControl('', {}),
    actions: new FormControl('', {}),
    leaderCheckbox: new FormControl('', {}),
    groupId: new FormControl('', {
      validators: Validators.required
    }),
  });
  member: Member = new Member();

  private formSubmitAttempt: boolean;


  groupList: Group[] = [];
  id: number;

  rolesList: Role[];

  ngOnInit() {
    debugger;

    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      if (this.id) {
        // In a real app: dispatch action to load the details here.
        this.memberService.getMember(this.id).subscribe(
          data => {
            this.member = data;

            this.form = this.formBuilder.group({
              id: this.member.id,
              username: this.member.username,
              password: this.member.password,
              firstname: this.member.firstname,
              lastname: this.member.lastname,
              city: this.member.city,
              address: this.member.address,
              group: this.member.group,
              groupId: this.member.group.id,
              birthDate: this.member.birthDate,
              mobile: this.member.mobile,
              email: this.member.email,
              note: this.member.note,
              vegetarian: this.member.vegetarian,
              registrationCard: this.member.registrationCard,
              photoAgree: this.member.photoAgree,
              roles: this.member.roles,
              actions: this.member.actions,
              leaderCheckbox: false,
            });

            debugger;
            if (this.member.roles.find(i => i.code == 'ROLE_LEADER')) {
              this.form.controls.leaderCheckbox.setValue(true);
            } else {
              this.form.controls.leaderCheckbox.setValue(false);
            }

            if (this.isUser) {
              this.form.disable();
            }

          }
        );
      }
    });

    this.groupsService.getAllGroups().subscribe(
      data => {
        this.groupList = data;
      }
    );

    this.rolesService.getAllRoles().subscribe(
      data => {
        this.rolesList = data;
      }
    );

    debugger;
  }

  onSubmit() {
    debugger;

    this.form.controls.group.setValue(this.groupList.find(gr => gr.id == this.form.controls.groupId.value));
    if (this.form.valid) {


      if (this.form.controls.leaderCheckbox.value) {
        this.form.controls.roles.setValue([this.rolesList.find(role => role.code == 'ROLE_LEADER')]);
        //this.form.controls.roles.value.push(this.rolesList.find( role => role.code == 'ROLE_LEADER'))
        //this.form.controls.roles.value.splice(this.rolesList.indexOf( this.rolesList.find(role => role.code == 'ROLE_USER')), 1);
      } else {
        this.form.controls.roles.setValue([this.rolesList.find(role => role.code == 'ROLE_USER')]);
        //this.form.controls.roles.value.push(this.rolesList.find( role => role.code == 'ROLE_USER'))
        //this.form.controls.roles.value.splice(this.rolesList.indexOf( this.rolesList.find(role => role.code == 'ROLE_LEADER')), 1);
      }
      debugger;
      if (!this.form.controls.id.value) {
        debugger;
        this.memberService.saveMember(this.form.getRawValue()).subscribe(
          data => {
            this.router.navigate(['/clenove']);
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.memberService.updateMember(this.form.getRawValue()).subscribe(
          data => {
            this.router.navigate(['/clenove']);
          },
          error => {
            console.log(error);
          }
        );
      }
    }

    this.formSubmitAttempt = true;

    debugger;
  }

  deleteItem() {
    debugger;
    this.memberService.deleteMember(this.form.controls.id.value).subscribe(
      data => {
        this.router.navigate(['/clenove']);
      },
      error => {
        console.log(error);
      }
    );
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

}
