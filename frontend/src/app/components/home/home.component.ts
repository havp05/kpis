import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';

import {TokenStorageService} from '../../auth/token-storage.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {ActionsService} from '../../services/actions.service';
import {MembersService} from '../../services/members.service';
import {Member} from '../../Model/member';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  currentUser: Member;

  displayedColumns: string[] = ['name', 'region', 'startDate', 'endDate', 'address', 'signIn', 'maxPeople'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private tokenStorage: TokenStorageService, private membersService: MembersService, private actionsService: ActionsService, private router: Router, private changeDetectorRefs: ChangeDetectorRef) {
    this.findCurrentUser();
    debugger;
  }

  ngOnInit() {
    debugger;
    this.actionsService.getActionsByUserId(this.tokenStorage.getUsername()).subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeDetectorRefs.detectChanges();
        debugger;
      }
    );
    debugger;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  findCurrentUser() {
    debugger;
    this.membersService.getAllMembers().subscribe(
      data => {
        debugger;
        this.currentUser = data.find(m => m.username == this.tokenStorage.getUsername());
      }
    );

  }

}
