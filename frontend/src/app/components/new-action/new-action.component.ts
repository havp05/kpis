import {Component, OnInit, ViewChild} from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import {ActionsService} from '../../services/actions.service';

import {MembersService} from '../../services/members.service';
import {RegionsService} from '../../services/regions.service';
import {Action} from '../../Model/action';
import {Region} from '../../Model/region';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Member} from '../../Model/member';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {isArray} from 'util';


@Component({
  selector: 'app-new-action',
  templateUrl: './new-action.component.html',
  styleUrls: ['./new-action.component.scss']
})
export class NewActionComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(),
    name: new FormControl('', {
      validators: Validators.required,
    }),
    startDate: new FormControl('', {
      validators: Validators.required,
    }),
    endDate: new FormControl('', {
      validators: Validators.required
    }),
    signEndDate: new FormControl('', {}),
    maxPeople: new FormControl('', {
      validators: Validators.required,
    }),
    memberPrice: new FormControl('', {
      validators: Validators.required,
    }),
    nonMemberPrice: new FormControl('', {
      validators: Validators.required
    }),
    description: new FormControl('', {}),
    contactPerson: new FormControl('', {
      validators: Validators.required
    }),
    region: new FormControl('', {
      validators: Validators.required
    }),
    address: new FormControl('', {
      validators: Validators.required
    }),
    members: new FormControl('', {}),
    regionId: new FormControl('', {
      validators: Validators.required
    }),
  });

  private formSubmitAttempt: boolean;

  action: Action;
  regionList: Region[];
  id: number;
  isCurrentUserLoggedAction: boolean;

  currentUser: Member = new Member();

  isUser: boolean = false;

  displayedColumns: string[] = ['fullName', 'birthDate', 'group'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private membersService: MembersService, private regionsService: RegionsService,
              private actionsService: ActionsService, private router: Router,
              private route: ActivatedRoute, private tokenStorage: TokenStorageService,
              private formBuilder: FormBuilder,
  ) {

    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }

    this.findCurrentUser();
    debugger;
  }


  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      if (this.id) {
        // In a real app: dispatch action to load the details here.
        this.actionsService.getAction(this.id).subscribe(
          data => {
            this.action = data;

            this.form = this.formBuilder.group({
              id: this.action.id,
              name: this.action.name,
              startDate: this.action.startDate,
              endDate: this.action.endDate,
              region: this.action.region,
              signEndDate: this.action.signEndDate,
              description: this.action.description,
              regionId: this.action.region.id,
              maxPeople: this.action.maxPeople,
              memberPrice: this.action.memberPrice,
              nonMemberPrice: this.action.nonMemberPrice,
              members: this.action.members,
              contactPerson: this.action.contactPerson,
              address: this.action.address

            });

            this.dataSource = new MatTableDataSource(this.action.members);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;


            debugger;
            this.isCurrentUserLoggedAction = this.action.members.find(m => m.username == this.tokenStorage.getUsername()) != null;

            if (this.isUser) {
              this.form.disable();
            }

          }
        );
      }
    });


    this.regionsService.getAllRegions().subscribe(
      data => {
        this.regionList = data;
      }
    );


  }

  onSubmit() {
    debugger;
    this.form.controls.region.setValue(this.regionList.find(reg => reg.id == this.form.controls.regionId.value));
    if (this.form.controls.members.value == '') {
      this.form.controls.members.setValue([]);
    }
    if (this.form.valid) {

      this.actionsService.saveAction(this.form.getRawValue()).subscribe(
        data => {
          this.router.navigate(['/akce']);
        },
        error => {
          console.log(error);
        }
      );
    }
    this.formSubmitAttempt = true;
    debugger;
  }

  deleteItem() {
    this.actionsService.deleteAction(this.form.controls.id.value).subscribe(
      data => {
        this.router.navigate(['/akce']);
      },
      error => {
        console.log(error);
      }
    );
  }

  findCurrentUser() {
    debugger;
    this.membersService.getAllMembers().subscribe(
      data => {
        debugger;
        this.currentUser = data.find(m => m.username == this.tokenStorage.getUsername());
      }
    );

  }

  actionLogin() {
    debugger;
    let members: Member[];
    if (this.form.controls.members.value) {
      members = this.form.controls.members.value;
    } else {
      members = [];
    }

    members.push(this.currentUser);
    this.form.controls.members.setValue(members);

    debugger;
    this.actionsService.updateAction(this.form.getRawValue()).subscribe(
      data => {
        debugger;
        this.action = data;
        this.dataSource.data = this.action.members;
        this.form.controls.members.setValue(this.action.members);
      },
      error => {
        console.log(error);
      }
    );
    this.isCurrentUserLoggedAction = true;
  }

  actionLogout() {
    debugger;
    let members: Member[] = [];
    if (this.form.controls.members.value) {
      if (isArray(this.form.controls.members.value)) {
        members = this.form.controls.members.value;
      } else {
        members.push(this.form.controls.members.value);
      }
    } else {
      members = [];
    }
    debugger;
    let index = members.indexOf(this.currentUser);
    members.splice(index, 1);
    this.form.controls.members.setValue(members);

    this.actionsService.updateAction(this.form.getRawValue()).subscribe(
      data => {
        this.action = data;
        this.dataSource.data = this.action.members;
        this.form.controls.members.setValue(this.action.members);
      },
      error => {
        console.log(error);
      }
    );
    this.isCurrentUserLoggedAction = false;
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

}
