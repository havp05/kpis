import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {GroupsService} from '../../services/groups.service';
import {TokenStorageService} from '../../auth/token-storage.service';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'registrationYear', 'city', 'region', 'web', 'description'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isUser: boolean = false;

  constructor(private groupsService: GroupsService, private changeDetectorRefs: ChangeDetectorRef,
              private tokenStorage: TokenStorageService) {
    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }
  }

  ngOnInit() {


    this.groupsService.getAllGroups().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeDetectorRefs.detectChanges();
        debugger;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
