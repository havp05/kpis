import {Component, OnInit} from '@angular/core';
import {Group} from '../../Model/group';
import {ActivatedRoute, Router} from '@angular/router';
import {Region} from '../../Model/region';
import {GroupsService} from '../../services/groups.service';
import {RegionsService} from '../../services/regions.service';
import {TokenStorageService} from '../../auth/token-storage.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
})
export class NewGroupComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(),
    name: new FormControl('', {
      validators: Validators.required,
    }),
    registrationYear: new FormControl('', {}),
    city: new FormControl('', {
      validators: Validators.required
    }),
    region: new FormControl('', {
      validators: Validators.required
    }),
    web: new FormControl('', {}),
    description: new FormControl('', {}),
    regionId: new FormControl('', {
      validators: Validators.required
    }),
  });
  group: Group;

  private formSubmitAttempt: boolean;
  isUser: boolean = false;

  constructor(private tokenStorage: TokenStorageService, private router: Router, private route: ActivatedRoute, private groupService: GroupsService, private regionService: RegionsService, private formBuilder: FormBuilder) {
    if (tokenStorage.getMainRole() == 'user') {
      this.isUser = true;
    }
    debugger;
  }

  regionList: Region[];
  id: number;


  ngOnInit() {


    debugger;
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      if (this.id) {
        // In a real app: dispatch action to load the details here.
        this.groupService.getGroup(this.id).subscribe(
          data => {
            this.group = data;

            this.form = this.formBuilder.group({
              id: this.group.id,
              name: this.group.name,
              registrationYear: this.group.registrationYear,
              city: this.group.city,
              region: this.group.region,
              web: this.group.web,
              description: this.group.description,
              regionId: this.group.region.id
            });

            debugger;

            if (this.isUser) {
              this.form.disable();
            }


          }
        );
      }
    });

    this.regionService.getAllRegions().subscribe(
      data => {
        this.regionList = data;
      }
    );
    debugger;
  }

  onSubmit() {
    debugger;
    this.form.controls.region.setValue(this.regionList.find(reg => reg.id == this.form.controls.regionId.value));
    if (this.form.valid) {
      this.groupService.saveGroup(this.form.getRawValue()).subscribe(
        data => {
          this.router.navigate(['/oddily']);
        },
        error => {
          console.log(error);
        }
      );
    }
    this.formSubmitAttempt = true;
    debugger;
  }

  deleteItem() {
    this.groupService.deletegroup(this.form.controls.id.value).subscribe(
      data => {
        this.router.navigate(['/oddily']);
      },
      error => {
        console.log(error);
      }
    );
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

}
