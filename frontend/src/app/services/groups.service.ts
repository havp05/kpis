import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Group} from '../Model/group';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  private allGroupUrl = 'http://localhost:9000/oddily';

  constructor(private http: HttpClient) {
  }


  getAllGroups(): Observable<any> {
    return this.http.get<Array<any>>(this.allGroupUrl);
  }

  getGroup(id: number): Observable<any> {
    return this.http.get<Group>(this.allGroupUrl + '/' + id);
  }

  saveGroup(region: Group): Observable<string> {
    return this.http.post<string>(this.allGroupUrl, region, httpOptions);
  }

  deletegroup(id: number): Observable<string> {
    debugger;
    return this.http.delete<string>(this.allGroupUrl + '/' + id, httpOptions);
  }

}
