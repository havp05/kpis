import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Region} from '../Model/region';

@Injectable({
  providedIn: 'root'
})
export class RegionsService {

  private regionsURL = 'http://localhost:9000/oblasti';

  constructor(private http: HttpClient) {
  }


  getAllRegions(): Observable<any> {
    return this.http.get<Array<Region>>(this.regionsURL);
  }
}
