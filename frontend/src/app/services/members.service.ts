import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Member} from '../Model/member';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MembersService {

  private allMembersUrl = 'http://localhost:9000/users';


  constructor(private http: HttpClient) {
  }

  getAllMembers(): Observable<any> {
    debugger;
    return this.http.get<Array<any>>(this.allMembersUrl);
  }

  getAllMembersExceptSuperadmin(): Observable<any> {
    return this.http.get<Array<any>>(this.allMembersUrl + '/exceptsuperadmin');
  }

  getMember(id: number): Observable<any> {
    return this.http.get<Member>(this.allMembersUrl + '/' + id);
  }

  saveMember(member: any): Observable<string> {
    debugger;
    return this.http.post<string>(this.allMembersUrl, member, httpOptions);
  }

  updateMember(member: Member): Observable<any> {
    debugger;
    return this.http.put<Member>(this.allMembersUrl + '/' + member.id, member, httpOptions);
  }

  deleteMember(id: number): Observable<string> {
    return this.http.delete<string>(this.allMembersUrl + '/' + id, httpOptions);
  }
}
