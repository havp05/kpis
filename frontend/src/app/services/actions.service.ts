import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Action} from '../Model/action';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  constructor(private http: HttpClient) {
  }


  private actionsUrl = 'http://localhost:9000/akce';


  getAllActions(): Observable<any> {
    return this.http.get<Array<any>>(this.actionsUrl);
  }

  getAction(id: number): Observable<any> {
    return this.http.get<Action>(this.actionsUrl + '/' + id);
  }

  saveAction(action: any): Observable<string> {
    return this.http.post<string>(this.actionsUrl, action, httpOptions);
  }

  updateAction(action: Action): Observable<any> {
    debugger;
    return this.http.put<Action>(this.actionsUrl + '/' + action.id, action, httpOptions);
  }

  deleteAction(id: number): Observable<string> {
    return this.http.delete<string>(this.actionsUrl + '/' + id, httpOptions);
  }

  getActionsByUserId(username: string): Observable<any> {
    return this.http.get<Array<any>>(this.actionsUrl + '/userActions/' + username);
  }
}
