import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Role} from '../Model/role';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient) {
  }


  private mainUrl = 'http://localhost:9000/role';


  getAllRoles(): Observable<any> {
    return this.http.get<Array<Role>>(this.mainUrl);
  }

}
