# KPIS

Informační systém pro správu klubu

## Databáze

Aplikace používá systém PostgreSQL.
Údaje ke konfiguraci jsou v souboru application.yml.
Tabulky není potřeba vytvářen, o ně se postará Liquibase při spuštění aplikace.

## Server

Spustit jako Spring Boot aplikaci.  
`mvn clean spring-boot:run`  
Hlavní metoda v souboru DiplomaThesisApplication.java.

## Klient
###Instalace Angularu
`npm install -g @angular/cli `
###Instalace balíčků
`npm install`
###Spuštění aplikace
`ng serve` 

Aplikace je dostupná na adrese localhost:4200.  
Uživatelské jméno: admin  
Heslo: admin12345